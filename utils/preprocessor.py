import os
from nltk.stem import WordNetLemmatizer
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords


def get_text(filename):
    with open(filename, 'r') as file:
        return file.read()


def write_text(filename, text):
    with open(filename, 'w') as file:
        file.writelines(text)


def preprocess_text(text, tokenizer=RegexpTokenizer(r'\w+'), lemmatizer=WordNetLemmatizer()):
    tokens = tokenizer.tokenize(text)
    lemmas = [lemmatizer.lemmatize(token.lower(), pos='v') for token in tokens]
    keywords = [x for x in lemmas if x not in stopwords.words('english')]
    return keywords


def preprocess_text_lazy(text, tokenizer=RegexpTokenizer(r'\w+'), lemmatizer=WordNetLemmatizer()):
    def _lemmatize(word):
        result = lemmatizer.lemmatize(word.lower(), pos='n')
        return lemmatizer.lemmatize(result, pos='v')

    def _remove_stopwords(word):
        return word not in stopwords.words('english')

    return filter(_remove_stopwords, map(_lemmatize, tokenizer.tokenize(text)))


def scan_dir_content(dirname):
    for filename in os.scandir(dirname):
        if filename.is_file():
            try:
                path = filename.path
                yield filename.name, get_text(path)
            except UnicodeDecodeError:
                print(f'Failed to read {filename}')


def remove_special_characters(text):
    text = text.replace('\n', ' ')
    text = text.replace('\r', ' ')
    text = text.replace('\'', ' ')
    text = [sentence.strip() for sentence in text.split('.')]
    text = [sentence for sentence in text if sentence != '']
    return text


if __name__ == '__main__':
    directories = ['scripts', 'books']
    for directory in directories:
        for file_name, content in scan_dir_content(directory):
            processed = preprocess_text(content)
            write_text(f'./processed/{file_name}', '\n'.join(processed))
            print(f'Processed {file_name}', end='\n' * 3)

