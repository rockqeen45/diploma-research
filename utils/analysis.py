import string
from collections import Counter

import pandas as pd
from flair.data import Sentence
from flair.models import SequenceTagger
from sklearn.feature_extraction.text import TfidfVectorizer
from tqdm import tqdm

from utils.preprocessor import remove_special_characters, scan_dir_content, write_text


def vectorize(text, preprocessor):
    vectorizer = TfidfVectorizer(analyzer=preprocessor)
    data = pd.DataFrame(text, columns=['sentence'])
    data = vectorizer.fit_transform(data['sentence'])
    data = pd.DataFrame.sparse.from_spmatrix(data)
    col_map = {v: k for k, v in vectorizer.vocabulary_.items()}
    for col in data.columns:
        data.rename(columns={col: col_map[col]}, inplace=True)
    return data


def get_characters_distribution(tagger, raw_text):
    text = remove_special_characters(raw_text)
    names_draft = []
    for index, line in enumerate(tqdm(text)):
        sentence = Sentence(line)
        tagger.predict(sentence)
        for entity in sentence.get_spans('ner'):
            label = entity.get_label("ner").value
            if label == 'PER':
                name = entity.text.translate(str.maketrans('', '', string.punctuation))
                names_draft.append(name)
    main_characters = Counter(names_draft).most_common()
    return {k: v for k, v in main_characters}
