from flair.models import SequenceTagger

from tools.profanity_markup import ProfanityMarker
from utils.analysis import get_characters_distribution
from utils.preprocessor import scan_dir_content, write_text


def prepare_data_for_characters_distribution_model():
    tagger = SequenceTagger.load('ner-fast')
    directories = ['books']
    counter = 0
    for directory in directories:
        print(f'[{counter}] DIRECTORY STARTED {directory}')
        for file_name, content in scan_dir_content(f'../{directory}'):
            print(f'[{counter}] FILE STARTED {file_name}')
            processed = get_characters_distribution(tagger, content)
            processed = [f'{k}, {v}' for k, v in processed.items()]
            path = f'../processed_characters/{directory}/{file_name.replace(".txt", ".csv")}'
            write_text(path, '\n'.join(processed))
            counter += 1
            print(f'[{counter}] FILE FINISHED {file_name}')


def generate_marked_profanity_text():
    marker = ProfanityMarker('|-|')
    directories = ['scripts', 'books']
    counter = 0
    for directory in directories:
        print(f'[{counter}] DIRECTORY STARTED {directory}')
        for file_name, content in scan_dir_content(f'../{directory}'):
            print(f'[{counter}] FILE STARTED {file_name}')
            processed = marker.run_censure(content)
            path = f'../processed_profanity/{directory}/{file_name}'
            write_text(path, processed)
            counter += 1
            print(f'[{counter}] FILE FINISHED {file_name}')
            break


if __name__ == '__main__':
    prepare_data_for_characters_distribution_model()
