from typing import List
from better_profanity import profanity


class ProfanityMarker:
    def __init__(self, mark: str):
        self.mark = mark

    def run_markup(self, text: List[str]) -> List[str]:
        return [self._mark_single(sentence) for sentence in text]

    def run_censure(self, text: str):
        return profanity.censor(text, self.mark)

    def _mark_single(self, sentence: str):
        return f'{self.mark}{sentence}{self.mark}' if profanity.contains_profanity(sentence) else sentence


if __name__ == '__main__':
    marker = ProfanityMarker('|-|')
    print(marker.run_censure('Ayo Bitch'))
