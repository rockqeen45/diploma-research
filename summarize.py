import nltk
import string
from heapq import nlargest

from main import get_text


if __name__ == '__main__':
    text = '''
    Speech is human vocal communication using language. 
    Each language uses phonetic combinations of vowel and consonant sounds that form the sound of its words 
    (that is, all English words sound different from all French words, 
    even if they are the same word, e.g., "role" or "hotel"), 
    and using those words in their semantic character as words in the lexicon of a language 
    according to the syntactic constraints that govern lexical words' function in a sentence. 
    In speaking, speakers perform many different intentional speech acts, e.g., informing, declaring, asking, 
    persuading, directing, and can use enunciation, intonation, degrees of loudness, tempo, and other 
    non-representational or paralinguistic aspects of vocalization to convey meaning. 
    In their speech speakers also unintentionally communicate many aspects of their social position such as 
    sex, age, place of origin (through accent), physical states (alertness and sleepiness, vigor or weakness, 
    health or illness), psychological states (emotions or moods), physico-psychological states 
    (sobriety or drunkenness, normal consciousness and trance states), education or experience, and the like.
    Although people ordinarily use speech in dealing with other persons (or animals), 
    when people swear they do not always mean to communicate anything to anyone, 
    and sometimes in expressing urgent emotions or desires they use speech as a quasi-magical cause, 
    as when they encourage a player in a game to do or warn them not to do something. 
    There are also many situations in which people engage in solitary speech. 
    People talk to themselves sometimes in acts that are a development of what some psychologists (e.g., Lev Vygotsky) 
    have maintained is the use of silent speech in an interior monologue to vivify and organize cognition, 
    sometimes in the momentary adoption of a dual persona as self addressing self as though addressing another person. 
    Solo speech can be used to memorize or to test one's memorization of things, 
    and in prayer or in meditation (e.g., the use of a mantra).
    '''

    if text.count(". ") > 20:
        length = int(round(text.count(". ") / 10, 0))
    else:
        length = 1

    nopuch = [char for char in text if char not in string.punctuation]
    nopuch = "".join(nopuch)

    processed_text = [word for word in nopuch.split() if word.lower() not in nltk.corpus.stopwords.words('english')]

    word_freq = {}
    for word in processed_text:
        if word not in word_freq:
            word_freq[word] = 1
        else:
            word_freq[word] = word_freq[word] + 1

    max_freq = max(word_freq.values())
    for word in word_freq.keys():
        word_freq[word] = (word_freq[word]/max_freq)

    sent_list = nltk.sent_tokenize(text)
    sent_score = {}
    for sent in sent_list:
        for word in nltk.word_tokenize(sent.lower()):
            if word in word_freq.keys():
                if sent not in sent_score.keys():
                    sent_score[sent] = word_freq[word]
                else:
                    sent_score[sent] = sent_score[sent] + word_freq[word]

    summary_sents = nlargest(length, sent_score, key=sent_score.get)
    summary = "\n".join(summary_sents)
    print(summary)
