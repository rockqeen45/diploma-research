from utils.analysis import vectorize
from utils.preprocessor import get_text, preprocess_text, preprocess_text_lazy

if __name__ == '__main__':
    speech = get_text('books/Book-Coraline.txt')
    processed_data = vectorize([speech], preprocess_text_lazy)
    print(processed_data)
